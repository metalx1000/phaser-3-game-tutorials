const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 800,
  height: 600,
  scene: {
    preload: preload,
    update: update,
    create: create
  }
};

var sprite;
const game = new Phaser.Game(config);

function preload (){
  this.load.image('mushroom', 'res/mushroom2.png');  
}

function create (){
  sprite = this.add.image(game.canvas.width / 2, game.canvas.height /2, 'mushroom' ); 

  //follow mouse
  this.input.on('pointerdown', function (pointer) {
    sprite.x = pointer.x;
    sprite.y = pointer.y;
  });
}

function update(){

}
