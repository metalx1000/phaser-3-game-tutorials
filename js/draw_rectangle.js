const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 800,
  height: 600,
  scene: {
    preload: preload,
    update: update,
    create: create
  }
};

const game = new Phaser.Game(config);
var rectangle, graphics;

function preload (){

}

function create (){
  rectangle = new Phaser.Geom.Rectangle(200, 150, 300, 200);

  graphics = this.add.graphics();

}

function update(){
  graphics.clear();
  graphics.lineStyle(2, 0xff0000);
  graphics.strokeRectShape(rectangle);
}
