const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 800,
  height: 600,
  scene: {
    preload: preload,
    update: update,
    create: create
  }
};

const game = new Phaser.Game(config);
var mouse_cir, circles=[], color, graphics;
var radius = 50;

function preload (){

}

function create (){

  graphics = this.add.graphics();
  mouse_cir = new Phaser.Geom.Point(400, 300);

  //set inital color
  color = new Phaser.Display.Color();
  color.random(50);  

  //on mouse up 
  this.input.on('pointerup', function (pointer) {
    //change color
    color.random(50);  
    circles.push (new Phaser.Geom.Point(pointer.x, pointer.y));
    var len = circles.length;
  });

  //follow mouse
  this.input.on('pointermove', function (pointer) {
    mouse_cir.x = pointer.x;
    mouse_cir.y = pointer.y;
  });
}

function update(){
  graphics.clear();
  //graphics.lineStyle(4, 0xff0000 ,1);
  graphics.lineStyle(4, color.color ,1);
  for ( var circle of circles ){
    graphics.strokeCircle(circle.x, circle.y, radius);
  }
  graphics.strokeCircle(mouse_cir.x, mouse_cir.y, radius);

}
