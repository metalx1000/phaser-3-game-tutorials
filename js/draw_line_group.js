const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 800,
  height: 600,
  scene: {
    preload: preload,
    update: update,
    create: create
  }
};

const game = new Phaser.Game(config);
var lines = [];
var graphics;

function preload (){

}

function create (){

  graphics = this.add.graphics();
  for ( var i = 0;i <= 10;i++ ){
    lines.push( new Phaser.Geom.Line(100*i, 100*i, 20*i, 20*i) );
    lines[i].speed = i * .01;
  }

}

function update(){
  graphics.clear();
  graphics.lineStyle(20, 0x00ff00);
  for ( line of lines ){
    Phaser.Geom.Line.Rotate(line, line.speed);
    graphics.strokeLineShape(line); 
  }
}
