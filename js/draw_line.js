const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 800,
  height: 600,
  scene: {
    preload: preload,
    update: update,
    create: create
  }
};

const game = new Phaser.Game(config);
var line, line2, graphics;

function preload (){

}

function create (){

  graphics = this.add.graphics();
  line = new Phaser.Geom.Line(260, 200, 450, 450);
  line2 = new Phaser.Geom.Line(360, 200, 550, 450);

}

function update(){
  graphics.clear();
  Phaser.Geom.Line.Rotate(line, 0.02);
  Phaser.Geom.Line.Rotate(line2, 0.2);
  graphics.lineStyle(20, 0x00ff00);
  graphics.strokeLineShape(line); 
  graphics.strokeLineShape(line2); 
}
