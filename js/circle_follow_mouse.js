const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 800,
  height: 600,
  scene: {
    preload: preload,
    update: update,
    create: create
  }
};

const game = new Phaser.Game(config);
var circle, color, graphics;
var radius = 50;

function preload (){

}

function create (){

  graphics = this.add.graphics();
  circle = new Phaser.Geom.Point(400, 300);

  //set inital color
  color = new Phaser.Display.Color();
  color.random(50);  

  //on mouse down
  this.input.on('pointerup', function (pointer) {
    //change color
    color.random(50);  
  });

  //follow mouse
  this.input.on('pointermove', function (pointer) {
    circle.x = pointer.x;
    circle.y = pointer.y;
  });
}

function update(){
  graphics.clear();
  //graphics.lineStyle(4, 0xff0000 ,1);
  graphics.lineStyle(4, color.color ,1);
  graphics.strokeCircle(circle.x, circle.y, radius);

}
