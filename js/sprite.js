const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 800,
  height: 600,
  scene: {
    preload: preload,
    update: update,
    create: create
  }
};

var sprites=[];
const game = new Phaser.Game(config);

function preload (){
  this.load.image('mushroom', 'res/mushroom2.png');  
}

function create (){
  //diagonal line
  // for(var i = 0 ;i <= 10 ; i++){
    //   sprites.push(this.add.image(50*i, 50 * i, 'mushroom'));
    // }

    //grid
    var i = 100;
    for(var x = 0 ;x <= 10 ; x++){
      for(var y = 0 ;y <= 5 ; y++){
        sprites.push(this.add.image(i*x, i * y + 50, 'mushroom'));
      }
    }
}

function update(){

}
