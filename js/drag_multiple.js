const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 800,
  height: 600,
  scene: {
    preload: preload,
    update: update,
    create: create
  }
};

var sprites = [];
const game = new Phaser.Game(config);

function preload (){
  this.load.image('mushroom', 'res/mushroom2.png');  
}

function create (){
  for(var i = 0;i<=5;i++){
    sprites.push(this.add.image(i * 150 + 100, game.canvas.height /2, 'mushroom' ).setInteractive());
    this.input.setDraggable(sprites[i]);
  }

  this.input.on('drag', function (pointer, gameObject, dragX, dragY) {
    gameObject.x = dragX;
    gameObject.y = dragY;
  });

}

function update(){

}
