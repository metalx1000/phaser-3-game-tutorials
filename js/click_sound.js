const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 800,
  height: 600,
  scene: {
    preload: preload,
    update: update,
    create: create
  }
};

var sprite, drum;
const game = new Phaser.Game(config);

function preload (){
  this.load.image('mushroom', 'res/mushroom2.png');  
  this.load.audio('drum', [ 'res/drum.mp3', 'res/drum.ogg' ]);
}

function create (){
  sprite = this.add.image(game.canvas.width / 2, game.canvas.height /2, 'mushroom' ).setInteractive(); 
  sprite.on('pointerdown', function (pointer) {
    drum.play();
    sprite.scale=1.2;
  })

sprite.on('pointerup', function (pointer) {
    sprite.scale=1;
  })


  drum = this.sound.add('drum');
}

function update(){

}
