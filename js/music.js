const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 800,
  height: 600,
  scene: {
    preload: preload,
    update: update,
    create: create
  }
};

var sprite, music, play = 0;
const game = new Phaser.Game(config);

function preload (){
  this.load.image('mushroom', 'res/mushroom2.png');  
  this.load.audio('music', [ 'res/music.mp3', 'res/music.ogg' ]);
}

function create (){
  sprite = this.add.image(game.canvas.width / 2, game.canvas.height /2, 'mushroom' ).setInteractive(); 
  sprite.on('pointerdown', function (pointer) {
    if ( play == 0 ){
      play = 1;
      music.pause();
    }else{
      play = 0;
      music.resume();
    }
  })

  music = this.sound.add('music');
  music.loop = true;
  music.play();
}

function update(){

}
