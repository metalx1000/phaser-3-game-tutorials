const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 800,
  height: 600,
  scene: {
    preload: preload,
    update: update,
    create: create
  }
};

const game = new Phaser.Game(config);
var circle, graphics;
var radius = 10;

function preload (){

}

function create (){

  graphics = this.add.graphics();

}

function update(){
  //graphics.clear();
  var color = new Phaser.Display.Color();
  color.random();
  //graphics.lineStyle(4, 0xff0000 ,1);
  graphics.lineStyle(4, color.color ,1);
  circle = new Phaser.Geom.Point(400, 300);
  graphics.strokeCircle(circle.x, circle.y, radius);
  radius++;

}
